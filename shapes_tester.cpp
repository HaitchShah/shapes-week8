#include <iostream>
#include "shapes.h"
#include <vector>


int main(){
  std::vector<Shape*> shapes;
  shapes.push_back(new Circle(10));
  shapes.push_back(new Square(5));
  shapes.push_back(new Rectangle(10, 10));

  for(Shape* s : shapes){
    std::cout << s->description() << std::endl;
  }

  for(Shape* s : shapes){
    delete s;
  }

  shapes.clear();
  
  return 0;
}
