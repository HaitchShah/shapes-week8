#include "shapes.h"

// Shape Base Class
Shape::Shape(std::string name)
{
  this->name = name;
}

Shape::~Shape()
{
}

std::string Shape::description()
{
  return name + " has area: " + std::to_string(area());
}


// Shape->Square Derived Class
Square::Square(double width) : Shape("Square")
{
  this->width = width;
}

double Square::area()
{
  return width * width;
}


// Shape->Rectangle Derived Class
Rectangle::Rectangle(double width, double height) : Shape("Rectangle")
{
  this->width = width;
  this->height = height;
}

double Rectangle::area(){
  return width*height;
}


// Shape->Circle Derived Class
Circle::Circle(double radius) : Shape("Circle")
{
  this->radius = radius;
}

double Circle::area(){
  return (3.141592 * (radius * radius));
}
